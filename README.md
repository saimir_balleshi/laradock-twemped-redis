# Laradock Twemped Redis



Is a laradock module which put a twemproxy service with redis instances besides it.


### Installation



```sh
$ cd inside_laradock_folder
$ mkdir twemped_redis
$ cd twemped_redis
$ git init
$ git remote add https://bitbucket.org/saimir_balleshi/laradock-twemped-redis.git
$ git pull origin master
```

Add below content to the end of the docker-compose.yml.

```sh
### TWEMPED_REDIS ###################################################
    twemped_redis:
      build:
        context: ./twemped_redis
      volumes:
        - ${DATA_PATH_HOST}/twemped_redis/data:/data
      ports:
        - "${TWEMPROXY_MASTERS_PORT}:5001"
        - "${TWEMPROXY_SLAVES_PORT}:5002"
      networks:
        - frontend
        - backend
```

To the .env file add below content to the end 

```sh
### TWEMP_REDIS #############################################

TWEMPROXY_MASTERS_PORT=5001
TWEMPROXY_SLAVES_PORT=5002
```
### Run the container

```sh
docker-compose  up  -d  twemped_redis
```
