FROM saimirballeshi/twemped_redis

LABEL maintainer="Saimir Balleshi <saimir.balleshi@gmail.com>"

ADD entrypoint.sh /home/entrypoint.sh
RUN chmod +x /home/entrypoint.sh

CMD ["/home/entrypoint.sh", "-d"]

EXPOSE ${TWEMPROXY_MASTERS_PORT}
EXPOSE ${TWEMPROXY_SLAVES_PORT}