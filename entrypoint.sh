#!/bin/ash
/usr/local/bin/redis-server /etc/redis.conf &
/usr/local/bin/redis-server /etc/redis2.conf &
/usr/local/bin/redis-server /etc/redis3.conf &
/usr/local/bin/redis-server /etc/redis4.conf &
nutcracker -c /etc/twemproxy/nutcracker.yml &

if [[ $1 == "-d" ]]; then
  while true; do sleep 1000; done
fi

if [[ $1 == "-bash" ]]; then
  /bin/bash
fi
